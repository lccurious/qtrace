#ifndef LARVACHART_H
#define LARVACHART_H

#include <QWidget>
#include <QtCharts>
#include <QtCharts/QChart>


namespace Ui {
class LarvaChart;
}

class LarvaChart : public QWidget
{
    Q_OBJECT

public:
    explicit LarvaChart(QWidget *parent = 0);
    ~LarvaChart();

    int writeData();

private:
    Ui::LarvaChart *ui;
    QChart *mChart;
    QSplineSeries *mSeries;

    // statistic move
    uint cntT = 0;
    static const uint sampleCount = 2000;
    QVector<QPointF> mBuffer;

    // drawing part
    QValueAxis *axisX, *axisY;
};

#endif // LARVACHART_H
