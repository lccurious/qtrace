#ifndef TRACER_H
#define TRACER_H

#include <iostream>
#include <dlib/data_io.h>
#include <dlib/dnn.h>
#include <dlib/image_processing.h>
#include <dlib/opencv.h>
#include <opencv2/opencv.hpp>
#include <string>
#include "larvachart.h"


class Tracer
{
public:
    Tracer();
    Tracer(const char* VideoName);

    ///
    /// \brief videoShow, the main windows to show the major processed frame
    ///
    void videoShow();

    ///
    /// \brief larvaContours, find the contours of larva
    ///
    void larvaContours();

    ///
    /// \brief larvaRect select the
    /// \param rRect
    /// \param ROI
    ///
    void larvaRect(cv::RotatedRect& rRect);

    ///
    /// \brief larvaLandmarks
    ///
    void larvaLandmarks();

    ///
    /// \brief analyisHist
    /// \param image
    ///
    void analyisHist(cv::Mat& image);

    ///
    /// \brief selectPoints
    ///
    void selectPoints();

    ///
    /// \brief MST, find the minimun spanning tree with given points
    /// \param inPoints
    /// \return ordered mst point sequence
    ///
    std::vector<cv::Point> MST(std::vector<cv::Point> inPoints);

    void voronoiDiagram(cv::InputArray inFrame);

    std::vector<cv::Point> controlPoints(std::vector<cv::Point> inPoints);

    std::vector<cv::Point> getOmegaSet();

    ///
    /// \brief HeadTale
    /// \param contour
    /// \return
    /// \authors GeYouheng
    std::vector<int> HeadTale(std::vector<cv::Point> contour);

    ///
    /// \brief smoothCurve
    /// \param points
    /// \return
    /// \author GeYouheng
    std::vector<cv::Point> smoothCurve(std::vector<cv::Point> contours);

    ///
    /// \brief smoothTrace
    /// \param points
    /// \return
    /// \author GeYouheng
    std::vector<cv::Point> smoothTrace(std::vector<cv::Point> points);

    std::vector<cv::Point> splitContour(std::vector<cv::Point> contour, std::vector<int> HT);

    void landmarkShow();

private:
    // camera
    cv::VideoCapture cap;
    int w_width, w_height, re_width, re_height;
    cv::Size frameSize;

    // mouse interactive
    int clickedX, clickedY;
    int frame_cnt = 0;

    // global frame
    std::string mainWinName;
    cv::Mat mCurrentI, mContoursI, mGrayI;
    cv::Mat mLarvaMask, fgMaskMOG2;
    cv::Rect mLarvaBound;
    cv::Point mInitial[2];
    cv::Ptr<cv::BackgroundSubtractor> pBGS;

    // warpaffined ROI
    cv::Mat currentROI;
    // warpaffined transform matrix

    // Dlib
    dlib::shape_predictor predictor;
    dlib::full_object_detection currentS;
    int points_direc[22] = {0, 11, 15, 16, 17, 18, 19, 20, 21, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14};

    // frame smooth
    std::vector<cv::Mat> frameBuffer;
    std::vector<cv::Rect> larvaEstRects;

    // MST path parameters
    std::vector<cv::Point> mstList;
    std::vector<cv::Point> cPoints;

    /// {@ MST utility function
    double Edistance(cv::Point aPos, cv::Point bPos)
    {
        return sqrt((aPos.x - bPos.x)*(aPos.x - bPos.x)+
                    (aPos.y - bPos.y)*(aPos.y - bPos.y));
    }
    struct MSTEdge{
        int u, v;
        double cost;
        MSTEdge(int x, int y, double c):u(x),v(y),cost(c){}
        // for sort implement
        friend bool operator < (const MSTEdge& lhs, const MSTEdge& rhs)
        {
            return lhs.cost < rhs.cost;
        }
    };
    int findFather(std::vector<int> father, int x)
    {
        int a = x;
        while(x != father[x])
        {
            x = father[x];
        }
        while(a != father[a])
        {
            int z = a;
            a = father[a];
            father[z] = x;
        }
        return x;
    }
    /// @}

    // show statistic
    LarvaChart* mChart;

    // Contours finder;
    void larvaContours(cv::InputArray image, std::vector<std::vector<cv::Point> >& contours);
};

#endif // TRACER_H
