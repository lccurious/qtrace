#include "mainwindow.h"
#include "ui_mainwindow.h"


cv::Point originPoint;
cv::Point processPoint;
bool leftButtonDownFlag=false;

cv::Rect targetROI;
cv::Mat targetImage;
cv::Mat gCurrentFrame;
bool kcfinit = true;


void onMouse(int event, int x, int y, int, void *)
{
    cv::Mat imageCopy;
    if (event == cv::EVENT_LBUTTONDOWN)
    {
        leftButtonDownFlag = true;
        originPoint = cv::Point(x, y);
        processPoint = originPoint;
        // qDebug() << "onMouse trigged";
    }
    if (event == cv::EVENT_MOUSEMOVE&&leftButtonDownFlag)
    {
        processPoint = cv::Point(x, y);
        imageCopy = gCurrentFrame.clone();
        if (originPoint != processPoint) {
            cv::rectangle(imageCopy, originPoint, processPoint, cv::Scalar(255, 0, 0), 2);
        }
        cv::imshow("Meta", imageCopy);
    }
    if (event == cv::EVENT_LBUTTONUP)
    {
        targetROI = cv::Rect(originPoint, processPoint);
        // qDebug() << "onMouse Up";
        leftButtonDownFlag = false;
        kcfinit = true;
        qDebug() << "KCF init: " << kcfinit;
    }
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->statusBar()->messageChanged(QString("Welcome to QTrace 0.1"));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::saveToFile()
{

}

void MainWindow::currentFrame(cv::Mat &frameContainer)
{
    if (capture.isOpened()) {
        frameContainer = gCurrentFrame.clone();
    }
}

void MainWindow::dynamicSub(cv::InputArray)
{
}

void MainWindow::loadFromFile(int videoID)
{
    cv::Mat frame;

    // traditional method parameters
    int gapTime = 30;

    int resize_w, resize_h;

    capture = cv::VideoCapture(videoID);

    if (!capture.isOpened()) {
        qDebug() << "camera: camera error";
        return;
    }
    capture.read(frame);

    resize_w = frame.cols;
    resize_h = frame.rows;

    /// todo: with adaptive window size may not need to resize
    while (resize_h > 600 || resize_w > 800) {
        resize_h = resize_h >> 1;
        resize_w = resize_w >> 1;
    }

    cv::resize(frame, gCurrentFrame, cv::Size(resize_w, resize_h));

    qDebug() << "channel is" << frame.channels();

    cv::namedWindow("Meta", cv::WINDOW_AUTOSIZE);
    cv::setMouseCallback("Meta", onMouse);
    cv::imshow("Meta", gCurrentFrame);

    /// KCF tracking parameter
    cv::Ptr<cv::TrackerKCF> tracker = cv::TrackerKCF::create();
    cv::Rect2d trackWindow;
    /// End Of KCF para

    while (true) {

        if (!leftButtonDownFlag && !capture.read(frame) || cv::waitKey(gapTime) == 27) {
            qDebug() << "Leaving frame";
            // reset the ROI for next video.
            kcfinit = true;
            break;
        }
        if (!leftButtonDownFlag) {
            cv::imshow("Meta", gCurrentFrame);
        }
        cv::resize(frame, gCurrentFrame, cv::Size(resize_w, resize_h));
        if (targetROI.area() > 0 && !leftButtonDownFlag) {
            // qDebug() << "Refresh the ROI";

            /// todo: insert the roi tracking
            // set up the KCF search properties
            if (kcfinit) {
                tracker.release();
                tracker = cv::TrackerKCF::create();
                tracker->init(gCurrentFrame, targetROI);
                trace.clear();
                kcfinit = false;
            }

            tracker->update(gCurrentFrame, trackWindow);
            trace.push_back(cv::Point(trackWindow.x+trackWindow.width/2,
                                      trackWindow.y+trackWindow.height/2));
            for (int i = 0; i < trace.size()-1; i++) {
                cv::line(gCurrentFrame, trace[i], trace[i+1],
                        cv::Scalar(0,255,0), 2.5);
            }
            cv::rectangle(gCurrentFrame, trackWindow, cv::Scalar(255, 0, 0), 2);
        }

    }
    tracker.release();
    trace.clear();
    cv::destroyAllWindows();
    capture.release();
}

void MainWindow::on_actionOpen_triggered()
{
    filename = QFileDialog::getOpenFileName(
                this,
                "Open Video",
                QDir::currentPath(),
                "Video files (*.avi) ;; Video files (*.mp4) ;; All files (*.*)");

    if (!filename.isNull())
    {
        qDebug() << "select file path: " << filename.toUtf8();
        qDebug() << "ROI: " << targetROI.x
                 << "\t" << targetROI.y;
    }
}


void MainWindow::on_pbCamera_clicked()
{
    loadFromFile(0);
}

void MainWindow::on_pbTrace_clicked()
{
    filename = QFileDialog::getOpenFileName(
                this,
                "Open Video",
                QDir::cleanPath("F:\\MicroRecog\\Data\\20180323 放给欧阳\\20180322 优秀视频"),
                "Video files (*.avi) ;; Video files (*.mp4) ;; All files (*.*)");

    if (!filename.isNull())
    {
        qDebug() << "select file path: " << filename.toUtf8();
        qDebug() << "ROI: " << targetROI.x
                 << "\t" << targetROI.y;
        Tracer larvaT(filename.toStdString().c_str());
        larvaT.videoShow();
    }
}

void MainWindow::on_pbSelect_clicked()
{
}

void MainWindow::on_pbBone_clicked()
{
    filename = QFileDialog::getOpenFileName(
                this,
                "Open Video",
                QDir::cleanPath("F:\\MicroRecog\\Data\\20180323 放给欧阳\\20180322 优秀视频"),
                "Video files (*.avi) ;; Video files (*.mp4) ;; All files (*.*)");

    if (!filename.isNull())
    {
        qDebug() << "select file path: " << filename.toUtf8();
        qDebug() << "ROI: " << targetROI.x
                 << "\t" << targetROI.y;
        Tracer larvaT(filename.toStdString().c_str());
        larvaT.landmarkShow();
    }
}

void MainWindow::on_pbStatistic_clicked()
{

}
