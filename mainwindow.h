#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QFileDialog>
#include <QLineEdit>
#include <QTimer>
#include <QDebug>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <vector>
#include <opencv2/tracking.hpp>
#include "tracer.h"
#include "larvachart.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void saveToFile();
    void loadFromFile(int videoID);
    void currentFrame(cv::Mat& frameContainer);
    void dynamicSub(cv::InputArray);

    QPushButton* btn;
    QFileDialog* fd;
    QLineEdit* line;
    cv::VideoCapture capture;
    LarvaChart* lChart;

private slots:

    void on_actionOpen_triggered();

    void on_pbCamera_clicked();

    void on_pbTrace_clicked();

    void on_pbSelect_clicked();

    void on_pbBone_clicked();

    void on_pbStatistic_clicked();

private:
    Ui::MainWindow *ui;

    QTimer mTimer;

    QString filename;

    std::vector<cv::Point> trace;
};

#endif // MAINWINDOW_H
