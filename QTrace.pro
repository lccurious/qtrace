#-------------------------------------------------
#
# Project created by QtCreator 2018-04-12T08:23:50
#
#-------------------------------------------------

QT       += core gui charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QTrace
TEMPLATE = app


# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    tracer.cpp \
    larvachart.cpp

HEADERS += \
        mainwindow.h \
    tracer.h \
    larvachart.h

INCLUDEPATH += ..\dep\include

FORMS += \
        mainwindow.ui \
    larvachart.ui

# Opencv files
CONFIG(debug, debug|release) {
    LIBS += -L..\dep\lib \
            -lopencv_core340d \
            -lopencv_tracking340d \
            -lopencv_videoio340d \
            -lopencv_video340d \
            -lopencv_imgcodecs340d \
            -lopencv_highgui340d \
            -lopencv_imgproc340d \
            -lopencv_photo340d
} else {
    LIBS += -L..\dep\lib \
            -lopencv_core340 \
            -lopencv_tracking340 \
            -lopencv_videoio340 \
            -lopencv_video340 \
            -lopencv_imgcodecs340 \
            -lopencv_highgui340 \
            -lopencv_imgproc340 \
            -lopencv_photo340
}

# dlib files, only release contained
LIBS += ..\dep\lib\dlib19.10.0_release_64bit_msvc1913.lib
