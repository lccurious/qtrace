#include "larvachart.h"
#include "ui_larvachart.h"
#include <QtCharts/QSplineSeries>
#include <QtCharts/QValueAxis>
#include <QtCharts/QAbstractAxis>
#include <iostream>


QT_CHARTS_USE_NAMESPACE

LarvaChart::LarvaChart(QWidget *parent) :
    QWidget(parent),
    mChart(0),
    mSeries(0),
    ui(new Ui::LarvaChart)
{
    mChart = new QChart;
    QChartView *chartView = new QChartView(mChart);
    chartView->setMinimumSize(800, 600);
    mSeries = new QSplineSeries(this);

    // for dynamic drawing
    QPen linePen(Qt::red);
    linePen.setWidth(3);
    mSeries->setPen(linePen);

    mChart->addSeries(mSeries);
    axisX = new QValueAxis();
    axisX->setRange(0, 10);
    axisX->setLabelFormat("%g");
    axisX->setTitleText("Time");
    axisY = new QValueAxis();
    axisY->setRange(-1, 1);
    axisY->setTitleText("Wave");
    mChart->setAxisX(axisX, mSeries);
    axisX->setTickCount(15);
    mChart->setAxisY(axisY, mSeries);
    mChart->legend()->hide();
    mChart->setTitle("Data from Larva Moving");

    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addWidget(chartView);
    setLayout(mainLayout);

    ui->setupUi(this);
}

int LarvaChart::writeData()
{
    qreal x = mChart->plotArea().width() / axisX->tickCount();

    mSeries->append(cntT, (double)(rand()%1000-500)/1000.0);

    cntT++;

    mChart->scroll(x, 0);

    return cntT;
}

LarvaChart::~LarvaChart()
{
    delete ui;
}
