#include "tracer.h"

using std::vector;

int _CLICKED_X, _CLICKED_Y, _CLICK_CNT = 0;
int _CLICK_POINTS[4];

void CallBackFunc(int event, int x, int y, int flags, void* userdata)
{
     if  ( event == cv::EVENT_LBUTTONDOWN )
     {
          std::cout << "Left button of the mouse is clicked - position ("
                    << x << ", " << y << ")" << std::endl;
          _CLICKED_X = x;
          _CLICKED_Y = y;

          if (_CLICK_CNT == 0)
          {
              _CLICK_POINTS[0] = x;
              _CLICK_POINTS[1] = y;
              _CLICK_CNT++;
          }
          else if (_CLICK_CNT == 1)
          {
              _CLICK_POINTS[2] = x;
              _CLICK_POINTS[3] = y;
              _CLICK_CNT++;
          }
          else
          {
              _CLICK_POINTS[0] = x;
              _CLICK_POINTS[1] = y;
              _CLICK_CNT = 1;
          }
     }
     else if  ( event == cv::EVENT_RBUTTONDOWN )
     {
          std::cout << "Right button of the mouse is clicked - position ("
                    << x << ", " << y << ")" << std::endl;
     }
}

Tracer::Tracer()
{
    // initial dlib
    try{
        dlib::deserialize("knowledge/predictor_align.dat") >> predictor;
        std::cout << "Initialize predictor successful" << std::endl;
    }
    catch (std::exception e)
    {
        std::cout << "Initialize predictor failed" << std::endl;
        std::cout << e.what() << std::endl;
    }
}

Tracer::Tracer(const char* VideoName)
{
    // initialize dlib
    try{
        dlib::deserialize("knowledge/predictor_align.dat") >> predictor;
        std::cout << "Initialize predictor successful" << std::endl;
    }
    catch (std::exception e)
    {
        std::cout << "Initialize predictor failed" << std::endl;
        std::cout << e.what() << std::endl;
    }

    // initialize video capture
    try{
        cap.open(VideoName);
        w_width = cap.get(cv::CAP_PROP_FRAME_WIDTH);
        w_height = cap.get(cv::CAP_PROP_FRAME_HEIGHT);
        re_width = w_width;
        re_height = w_height;
        frame_cnt = cap.get(cv::CAP_PROP_FRAME_COUNT);
        while(re_width > 1600 || re_height > 1800)
        {
            re_width /= 2;
            re_height /= 2;
        }

        pBGS = cv::createBackgroundSubtractorMOG2();

        cap >> mCurrentI;

        pBGS->apply(mCurrentI, fgMaskMOG2);

        frameBuffer.push_back(mCurrentI);
        selectPoints();
        frameSize = mCurrentI.size();

        std::cout << "Video open successful" << std::endl;
    }
    catch (std::exception e)
    {
        std::cout << "Open Video failed" << std::endl;
        std::cout << e.what() << std::endl;
    }
}


void Tracer::selectPoints()
{
    _CLICK_CNT = 0;
    cv::Mat selectFrame;
    std::string selectInfo("please select tail");
    int key = 0;

    cv::resize(mCurrentI, selectFrame,cv::Size(re_width, re_height));
    cv::putText(selectFrame, selectInfo, cv::Point(5, 25), cv::FONT_ITALIC, 1.0, (0, 255, 2), 2.0);
    cv::namedWindow("videoShow");
    cv::setMouseCallback("videoShow", CallBackFunc, NULL);
    while (key != 27 || _CLICK_CNT != 2)
    {
        cv::imshow("videoShow", selectFrame);
        if (_CLICK_CNT == 1)
        {
            mInitial[0].x = _CLICK_POINTS[0];
            mInitial[0].y = _CLICK_POINTS[1];
            cv::circle(selectFrame, mInitial[0], 3, cv::Scalar(0, 125, 255), 2);
        }
        else if (_CLICK_CNT == 2)
        {
            mInitial[0].x = _CLICK_POINTS[0];
            mInitial[0].y = _CLICK_POINTS[1];
            mInitial[1].x = _CLICK_POINTS[2];
            mInitial[1].y = _CLICK_POINTS[3];
            cv::circle(selectFrame, mInitial[0], 3, cv::Scalar(0, 125, 255), 2);
            cv::circle(selectFrame, mInitial[1], 3, cv::Scalar(0, 255, 125), 2);
        }
        key = cv::waitKey(30);
    }
    cv::destroyWindow("videoShow");
}

void Tracer::videoShow()
{
    int width, height;
    width = w_width;
    height = w_height;
    cv::Mat outI;
    int key = 0;

    for (int i = 0; i < frame_cnt; i++)
    {
        cap >> mCurrentI;

        // processing frame
        larvaContours();

        cv::resize(mCurrentI, outI, cv::Size(re_width, re_height));
        cv::imshow("videShow", outI);

        key = cv::waitKey(30);
        if ((key & 0xff) == 27)
        {
            break;
        }
        else if ((key & 0xff) == 32)
        {

        }
    }
    cv::destroyAllWindows();
}

void Tracer::landmarkShow()
{
    cv::Mat outI, moveI;
    int key = 0;

    for (int i = 0; i < frame_cnt; i++)
    {
        cap >> mCurrentI;

        pBGS->apply(mCurrentI, fgMaskMOG2);

        // draw landmark
        larvaLandmarks();

        cv::resize(mCurrentI, outI, cv::Size(re_width, re_height));
        // cv::resize(fgMaskMOG2, moveI, cv::Size(re_width, re_height));
        cv::imshow("Bone", outI);
        // cv::imshow("Larva Part", moveI);

        key = cv::waitKey(30);
        if ((key & 0xff) == 27)
        {
            break;
        }
    }
    cv::destroyAllWindows();
}

std::vector<cv::Point> Tracer::getOmegaSet()
{
    cv::Scalar meanVal, stdDeviation;
    cv::Mat threshI, rectMask = cv::Mat::zeros(mCurrentI.size(), CV_8U);
    std::vector<cv::Point> omegaSet;

    cv::meanStdDev(mGrayI, meanVal, stdDeviation, mLarvaMask);

    // todo: it seems must be minus
    cv::threshold(mGrayI, threshI, meanVal[0] - stdDeviation[0], 255,cv::THRESH_BINARY_INV);

    cv::Mat bigOmega;
    rectMask(mLarvaBound) = 1;
    cv::bitwise_and(threshI, mGrayI, bigOmega, rectMask);

    int left, right, top, bottom;
    left = mLarvaBound.x;
    right = left+mLarvaBound.width;
    top = mLarvaBound.y;
    bottom = top + mLarvaBound.height;

    cv::Mat larvaROI = bigOmega(mLarvaBound);
    cv::Mat openKernel = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5));
    // cv::morphologyEx(bigOmega(mLarvaBound), larvaROI, cv::MORPH_OPEN, openKernel);

    // {@ debug
    cv::Mat outI;
    // cv::resize(larvaROI, outI, cv::Size(re_width, re_height));
    cv::imshow("bigOmegaSet", larvaROI);
    // std::cout << "mean value: " << meanVal[0]
    //           << "\tstd: " << stdDeviation[0] << std::endl;
    // @}

    for (int i = 0; i < larvaROI.rows; i++)
    {
        const uchar* Mi = larvaROI.ptr<uchar>(i);
        for (int j = 0; j < larvaROI.cols; j++)
        {
            if (Mi[j] > 0 && rand() % 100 < 30)
            {
                // std::cout << "[" << i << ", " << j << "]"
                //           << std::endl;
                omegaSet.push_back(cv::Point(left+j, top+i));
            }
        }
    }

    return omegaSet;
}

void Tracer::larvaRect(cv::RotatedRect& rRect)
{
    cv::Point2f pts[4];
    cv::Point2f nPts[4];
    cv::Point2f larvaCenter;
    cv::Mat warpI, warpedROI, invertWarp;
    vector<cv::Point> segments, segmentOrigins;
    cv::Point ROIoffset, ROIback1, ROIback2;
    int maxX = 0;
    int maxY = 0;
    int left, top, margin = 60;
    bool Rotate90 = false;

    rRect.points(pts);
    int height = 0, width = 0;

    larvaCenter = (pts[0] + pts[1] + pts[2] + pts[3]) / 4;
    cv::Mat M2 = cv::getRotationMatrix2D(larvaCenter, rRect.angle, 0.5);
    double* m = (double*)M2.data;
    cv::warpAffine(mCurrentI, warpI, M2, mCurrentI.size());
    for (int i = 0; i < 4; i++)
    {
        nPts[i].x = m[0]*pts[i].x + m[1]*pts[i].y + m[2];
        nPts[i].y = m[3]*pts[i].x + m[4]*pts[i].y + m[5];
    }
    for (int i = 0; i < 4; i++)
    {
        if (nPts[i].x - nPts[(i+2)%4].x > width)
        {
            width = nPts[i].x - nPts[(i+2)%4].x;
        }
        if (nPts[i].y - nPts[(i+2)%4].y > height)
        {
            height = nPts[i].y - nPts[(i+2)%4].y;
        }
        if (maxX < nPts[i].x) { maxX = nPts[i].x; }
        if (maxY < nPts[i].y) { maxY = nPts[i].y; }
    }

    // {@ Debug
    // for(int i = 0; i < 4; ++i)
    // {
    //     cv::line(warpI, nPts[i], nPts[(i + 1) % 4], cv::Scalar(255, 255, 0), 2);
    // }
    // @}

    // got the main roi corner with larva of the larva
    left = maxX-width-margin > 0 ? maxX-width-margin : 0;
    top = maxY-height-margin > 0 ? maxY-height-margin : 0;
    width = left+width+margin*2 < warpI.cols ? width+margin*2 : warpI.cols - left;
    height = top+height+margin*2 < warpI.rows ? height+margin*2 : warpI.rows - top;
    // select ROI
    cv::Rect rectROI(left, top, width, height);

    // std::cout << maxY << ", " << maxX << ", "
    //           << width << ", " << height << std::endl;

    warpedROI = warpI(rectROI);
    // to class wise
    currentROI = warpedROI;

    // rotate if the larva is not vertically selected
    if (height < width)
    {
        // cv::imshow("Before 90R", warpedROI);
        cv::rotate(warpedROI.clone(), warpedROI, cv::ROTATE_90_CLOCKWISE);
        maxX ^= maxY; maxY ^= maxX; maxX ^= maxY;
        width ^= height; height ^= width; width ^= height;
        Rotate90 = true;
    }

    // send it to dlib processing
    dlib::cv_image<dlib::bgr_pixel> cimg(warpedROI);
    dlib::rectangle dRect(0, 0, warpedROI.cols, warpedROI.rows);
    currentS = predictor(cimg, dRect);
    ROIoffset.x = left;
    ROIoffset.y = top;
    int dlibX, dlibY;
    for (int i = 0; i < 22; i++)
    {
        dlibX = currentS.part(points_direc[i]).x();
        dlibY = currentS.part(points_direc[i]).y();
        segments.push_back(cv::Point(dlibX, dlibY));
    }
    // draw contours respect to landmarks
    for (int i = 0; i < 22; i++)
    {
        cv::line(warpedROI, segments[i], segments[(i+1)%22], (255, 0, 255), 2);
    }
    // draw body segmentation respcet to landmarks
    // {@ debug
    for (int i = 1; i < 11; i++)
    {
        cv::line(warpedROI, segments[i], segments[(22-i)%22], (255, 0, 0), 2);
    }
    // @}
    cv::imshow("ROI", warpedROI);

    cv::Point pushP;

    // calculate inverse transformation matrix for larva
    cv::invertAffineTransform(M2, invertWarp);
    double* invertM = (double*)invertWarp.data;

    // inverse Affine Transform
    if (Rotate90)
    {
        std::cout << "Rotated" << std::endl;
    }

    // {@ Rectangle test
    ROIback1.x = invertM[0]*ROIoffset.x + invertM[1]*ROIoffset.y + invertM[2];
    ROIback1.y = invertM[3]*ROIoffset.x + invertM[4]*ROIoffset.y + invertM[5];
    ROIback2.x = invertM[0]*(ROIoffset.x+width) + invertM[1]*(ROIoffset.y+height) + invertM[2];
    ROIback2.y = invertM[3]*(ROIoffset.x+width) + invertM[4]*(ROIoffset.y+height) + invertM[5];

    cv::circle(mCurrentI, ROIback1, 4, (0, 255, 255), 4);
    cv::circle(mCurrentI, ROIback2, 4, (0, 255, 255), 4);
    // @}

    for (int i = 0; i < 22; i++)
    {
        pushP = ROIoffset+segments[i];
        pushP.x = invertM[0]*pushP.x + invertM[1]*pushP.y + invertM[2];
        pushP.y = invertM[3]*pushP.x + invertM[4]*pushP.y + invertM[5];
        segmentOrigins.push_back(pushP);
    }
    cv::Scalar color1(0, 255, 255), color2(0, 255, 2);
    // draw the contours into larva major body
    // {@ debug
    for (int i = 0; i < 22; i++)
    {
        cv::line(mCurrentI, segmentOrigins[i], segmentOrigins[(i+1)%22],
                color1, 4);
        cv::circle(mCurrentI, segmentOrigins[i], 6, color1, 3);
    }
    for (int i = 1; i < 11; i++)
    {
        cv::line(mCurrentI, segmentOrigins[i], segmentOrigins[(22-i)%22], color2, 4);
    }
    // @}
}

void Tracer::larvaContours()
{
    cv::Mat threshI, filtedI;
    vector<vector<cv::Point> > contours;
    vector<cv::Vec4i> hierarchy;
    double largestArea = 0.0;
    int largestAreaIdx = 0;
    cv::RotatedRect box;
    cv::Point2f pts[4];

    // todo: should be determined automatically
    cv::Mat closeKernel = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(25, 25));

    // smooth gray level
    cv::cvtColor(mCurrentI, mGrayI, cv::COLOR_BGR2GRAY);
    cv::bilateralFilter(mGrayI, filtedI, 11, 17, 17);
    // cv::blur(mGrayI, filtedI, cv::Size(50, 50));

    // {@ Adjust
    cv::threshold(filtedI, threshI, 200, 255, cv::THRESH_BINARY_INV);
    cv::morphologyEx(threshI, threshI, cv::MORPH_CLOSE, closeKernel);
    // @}

    threshI.copyTo(mLarvaMask);

    // No Canny needed
    // cv::Canny(threshI, edgeI, 30, 200);
    cv::findContours(threshI, contours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

    // get biggest area of contours
    // todo: keep top 5 area results
    for (int i = 0; i < contours.size(); i++)
    {
        double area = cv::contourArea(contours[i], false);
        if (area > largestArea)
        {
            largestArea = area;
            largestAreaIdx = i;
        }
    }
    mLarvaBound = cv::boundingRect(contours[largestAreaIdx]);
    // {@ Debug
    // cv::drawContours(mCurrentI, contours, largestAreaIdx,
    //                  cv::Scalar(0, 255, 0), 10);
    // @}

    // get most close rectangle include larva
    box = cv::minAreaRect(contours[largestAreaIdx]);
    std::cout << "rotation angle: " << box.angle << std::endl;
    box.points(pts);

    // {@ Debug
    // for(int i = 0; i < 4; ++i)
    // {
    //     cv::line(mCurrentI, pts[i], pts[(i + 1) % 4], cv::Scalar(255, 0, 0), 15);
    // }
    // @}

    // using MST method {@
    // vector<cv::Point> omegaSet = getOmegaSet();
    // for (int i = 0; i < contours[largestAreaIdx].size(); i++)
    // {
    //     if (rand() % 100 < 35)
    //     {
    //         omegaSet.push_back(contours[largestAreaIdx][i]);
    //     }
    // }
    // std::cout << "Points Set: " << omegaSet.size() << std::endl;
    // vector<cv::Point> contoursMST = MST(omegaSet);
    // int pointsPair = contoursMST.size();
    // for (int i = 1; i < pointsPair; i++)
    // {
    //     cv::line(mCurrentI, contoursMST[i-1], contoursMST[i], cv::Scalar(0, 255, 255), 2);
    //     // cv::circle(mCurrentI, contoursMST[i], 4, (0, 255, 0), cv::FILLED, CV_AA,0);
    //     // std::cout << contoursMST[i-1] << "->" << contoursMST[i] << std::endl;
    // }
    // cv::circle(mCurrentI, contoursMST[0],
    //         8, (255, 255, 255), cv::FILLED, CV_AA, 0);
    // cv::circle(mCurrentI, contoursMST.back(),
    //            8, (255, 255, 255), cv::FILLED, CV_AA, 0);
    // @}

    // use Ge method {@
    vector<cv::Point> Contour = contours[largestAreaIdx];
    vector<cv::Point> smoothContour = smoothCurve(contours[largestAreaIdx]);
    vector<vector<cv::Point> > smoothContours;
    smoothContours.push_back(smoothContour);
    vector<int> HTIdx = HeadTale(Contour);
    cv::circle(mCurrentI, Contour[HTIdx[0]],
            8, (0, 255, 255), cv::FILLED, CV_AA, 0);
    cv::circle(mCurrentI, Contour[HTIdx[1]],
            8, (0, 255, 255), cv::FILLED, CV_AA, 0);
    vector<cv::Point> midCurve = splitContour(Contour, HTIdx);
    cv::drawContours(mCurrentI, smoothContours, 0, (0, 255, 255), 2);
    // @}
}

void Tracer::larvaContours(cv::InputArray image, vector<vector<cv::Point> >& contours)
{
    cv::Mat threshI, filtedI, grayI;
    vector<cv::Vec4i> hierarchy;
    vector<vector<cv::Point> > rawContours;
    double largestArea = 0.0;
    int largestAreaIdx = 0;

    // todo: should be determined automatically
    cv::Mat closeKernel = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(25, 25));

    // smooth gray level
    cv::cvtColor(image, grayI, cv::COLOR_BGR2GRAY);
    cv::bilateralFilter(grayI, filtedI, 11, 17, 17);

    // {@ adjust to connect area
    cv::threshold(filtedI, threshI, 200, 255, cv::THRESH_BINARY_INV);
    cv::morphologyEx(threshI, threshI, cv::MORPH_CLOSE, closeKernel);
    // @}

    cv::findContours(threshI, rawContours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

    // get bigest area of contours;
    // todo: keep top 5 area results
    for (int i = 0; i < rawContours.size(); i++)
    {
        double area = cv::contourArea(rawContours[i], false);
        if (area > largestArea)
        {
            largestArea = area;
            largestAreaIdx = i;
        }
    }
    contours.push_back(rawContours[largestAreaIdx]);
}

vector<cv::Point> Tracer::splitContour(vector<cv::Point> contour, vector<int> HT)
{
    cv::Mat splitLarva(mCurrentI.clone());
    int Hidx = HT[0], Tidx = HT[1], size = contour.size();
    if (Hidx > Tidx)
    {
        int tmp = Hidx;
        Hidx = Tidx;
        Tidx = tmp;
    }
    vector<cv::Point> lBound, rBound;
    for (int i = Hidx+1; i < Tidx+1; i++)
    {
        cv::line(splitLarva, contour[i-1], contour[i], (0, 255, 0), 4);
        lBound.push_back(contour[i-1]);
    }
    for (int i = (Tidx+1); i < size+Hidx+1; i++)
    {
        cv::line(splitLarva, contour[(i-1)%size], contour[i%size], (0, 255, 255), 4);
        rBound.push_back(contour[(i-1)%size]);
    }
    int rNum = rBound.size(), lNum = lBound.size();
    int midNum = std::min(lNum, rNum);
    vector<cv::Point> midCurve;
    midCurve.push_back(rBound[0]);
    for (int i = 1; i < midNum; i++)
    {
        cv::line(splitLarva, (rBound[i-1]+lBound[lNum-i])/2, (rBound[i]+lBound[lNum-i-1])/2, (0, 255, 255), 4);

    }
    if (midNum == lNum)
    {
        cv::line(splitLarva, (rBound[midNum-2]+lBound[lNum-midNum+1])/2, rBound[rNum-1], (0, 240, 255), 4);
        midCurve.push_back(rBound[rNum-1]);
    }
    else if(midNum == rNum)
    {
        cv::line(splitLarva, (rBound[midNum-2]+lBound[lNum-midNum-+1])/2, lBound[0], (0, 240, 255), 4);
        midCurve.push_back(lBound[0]);
    }

    cv::Mat outI;
    cv::resize(splitLarva, outI,cv::Size(re_width, re_height));
    cv::imshow("Split", outI);
    return midCurve;
}

vector<cv::Point> Tracer::smoothCurve(vector<cv::Point> contours)
{
    // contour smoothing parameters for gaussian filter
    int filterRadius = 40;
    int filterSize = 2 * filterRadius + 1;
    double sigma = 20;

    // extract x and y coordinates of points. we'll consider these as 1-D signals
    // add circular padding to 1-D signals
    size_t len = contours.size() + 2 * filterRadius;
    size_t idx = (contours.size() - filterRadius);
    vector<float> x, y;
    for (size_t i = 0; i < len; i++) {
        x.push_back(contours[(idx + i) % contours.size()].x);
        y.push_back(contours[(idx + i) % contours.size()].y);
    }
    // filter 1-D signals
    vector<float> xFilt, yFilt;
    cv::GaussianBlur(x, xFilt, cv::Size(filterSize, filterSize), sigma, sigma);
    cv::GaussianBlur(y, yFilt, cv::Size(filterSize, filterSize), sigma, sigma);
    // build smoothed contour
    vector<cv::Point> smooth;
    for (size_t i = filterRadius; i < contours.size() + filterRadius; i++) {
        smooth.push_back(cv::Point(xFilt[i], yFilt[i]));
    }
    return smooth;
}

vector<cv::Point> Tracer::smoothTrace(vector<cv::Point> points)
{
    int filterRadius = 2;
    int filterSize = 2 * filterRadius + 1;
    double sigma = 5;

    vector<cv::Point>smoothedPoints;

    size_t len = points.size() + 2 * filterRadius;
    size_t idx = (points.size() - filterRadius);
    vector<float> x, y;
    for (size_t i = 0; i < len; i++) {
        x.push_back(points[(idx + i) % points.size()].x);
        y.push_back(points[(idx + i) % points.size()].y);
    }
    // filter 1-D signals
    vector<float> xFilt, yFilt;
    cv::GaussianBlur(x, xFilt, cv::Size(filterSize, filterSize), sigma, sigma);
    cv::GaussianBlur(y, yFilt, cv::Size(filterSize, filterSize), sigma, sigma);
    // build smoothed contour
    for (size_t i = filterRadius; i < points.size() + filterRadius; i++) {
        smoothedPoints.push_back(cv::Point(xFilt[i], yFilt[i]));
    }
    return smoothedPoints;
}

std::vector<int> Tracer::HeadTale(std::vector<cv::Point> contour)
{
    double minDis1 = 9999999, minDis2 = 9999999;
    int _IDX_A = 40;
    int size = contour.size();
    int minIdx1 = 0, minIdx2 = 0;

    vector<int> contourHT;
    for (int i = 0; i < size; i++)
    {
        double total = 0;
        for (int j = 1; j <= size / 8; j+=2)
        {
            total += Edistance(contour[(i+j) % size],
                    contour[(i-j+size)%size]);
        }
        if (total < minDis1)
        {
            minIdx1 = i;
            minDis1 = total;
        }
    }
    minIdx2 = (minIdx1 + size / 2) % size;
    for (int i = 0; i < size; i++)
    {
        double total = 0;
        for (int j = 1; j <= size / 8; j += 2)
        {
            total += Edistance(contour[(i+j) % size],
                    contour[(i-j+size)%size]);
        }
        if (total < minDis2)
        {
            if (fabs(i-minIdx2) < _IDX_A || fabs(i-minIdx2) > size - _IDX_A)
            {
                minIdx2 = i;
                minDis2 = total;
            }
        }
    }
    contourHT.push_back(minIdx1);
    contourHT.push_back(minIdx2);

    return contourHT;
}

vector<cv::Point> Tracer::MST(vector<cv::Point> inPoints)
{
    mstList.clear();
    int verN = inPoints.size();
    int numEdge = 0, bindEdge = 0;
    vector<int> father(verN);

    for (int i = 0; i < verN; i++)
    {
        father[i] = i;
    }

    // compute the distance between each point pair
    vector<MSTEdge> edgeDis;
    for (int i = 0; i < verN; i++)
    {
        for (int j = 0; j < verN; j++)
        {
            edgeDis.push_back(MSTEdge(i,j,Edistance(inPoints[i], inPoints[j])));
            numEdge ++;
        }
    }
    // sort the distance
    std::sort(edgeDis.begin(), edgeDis.end());

    mstList.push_back(inPoints[edgeDis[0].u]);
    for (int i = 0; i < numEdge; i++)
    {
        int faU = findFather(father, edgeDis[i].u);
        int faV = findFather(father, edgeDis[i].v);
        if (faU != faV)
        {
            father[faU] = faV;
            bindEdge++;
            mstList.push_back(inPoints[faV]);
            if (bindEdge == verN - 1) { break; }
        }
    }

    return mstList;
}

void Tracer::voronoiDiagram(cv::InputArray inFrame)
{
    std::string winVoronoi = "Voronoi Diagram";

    // Rectangle to be used with Subdiv2D
    cv::Rect rect(0, 0, frameSize.width, frameSize.height);

    // Create an instance of Subdiv2D
    cv::Subdiv2D subdiv(rect);

    vector<cv::Point2f> points;
    for (int i = 0; i < mstList.size(); i++)
    {
        points.push_back(cv::Point2f(mstList[i].x, mstList[i].y));
    }

    for (vector<cv::Point2f>::iterator it = points.begin(); it != points.end(); it++)
    {
        subdiv.insert(*it);
    }
}

void Tracer::analyisHist(cv::Mat& image)
{
    int histSize = 256;
    float range[] ={0, 256};
    vector<cv::Mat> bgrPlanes;
    cv::split(image, bgrPlanes);
    const float* histRange = {range};
    cv::Mat rHist, gHist, bHist;
    // cv::Mat HistMap(255, 100);
    cv::calcHist(&bgrPlanes[0], 1, 0, cv::Mat(),
            bHist, 1, &histSize, &histRange, true, false);
    cv::calcHist(&bgrPlanes[1], 1, 0, cv::Mat(),
            bHist, 1, &histSize, &histRange, true, false);
    cv::calcHist(&bgrPlanes[2], 1, 0, cv::Mat(),
            bHist, 1, &histSize, &histRange, true, false);

    int hist_w = 512, hist_h = 400;
    int bin_w = cvRound((double)hist_w/histSize);

    cv::Mat rHistMap(hist_h, hist_w, CV_8UC3, cv::Scalar(0, 0, 0));

    cv::normalize(bHist, bHist, 0, rHistMap.rows, cv::NORM_MINMAX, -1, cv::Mat());
    cv::normalize(gHist, gHist, 0, rHistMap.rows, cv::NORM_MINMAX, -1, cv::Mat());
    cv::normalize(rHist, rHist, 0, rHistMap.rows, cv::NORM_MINMAX, -1, cv::Mat());

    for (int i = 0; i < histSize; i++)
    {
        cv::line(bHist, cv::Point(bin_w*(i), hist_h-cvRound(bHist.at<float>(i))),
                 cv::Point(bin_w*(i), hist_h),
                 cv::Scalar(255, 0, 0), 2, 8, 0);
        cv::line(gHist, cv::Point(bin_w*(i), hist_h-cvRound(gHist.at<float>(i))),
                 cv::Point(bin_w*(i), hist_h),
                 cv::Scalar(0, 255, 0), 2, 8, 0);
        cv::line(rHist, cv::Point(bin_w*(i), hist_h-cvRound(rHist.at<float>(i))),
                 cv::Point(bin_w*(i), hist_h),
                 cv::Scalar(0, 0, 255), 2, 8, 0);
    }

}

void Tracer::larvaLandmarks()
{
    vector<vector<cv::Point> > larvaC;
    cv::Point2f pts[4];
    cv::RotatedRect box;
    cv::Rect larvaBound;
    cv::Mat larvaPart, moveI, mask;

    cv::bitwise_and(mCurrentI, mCurrentI, larvaPart, mask);

    cv::resize(larvaPart, moveI, cv::Size(re_width, re_height));
    cv::imshow("LarvaPart", moveI);

    larvaContours(larvaPart, larvaC);
    larvaBound = cv::boundingRect(larvaC[0]);
    box = cv::minAreaRect(larvaC[0]);
    box.points(pts);

    // Rect going to use dlib method
    larvaRect(box);
}
